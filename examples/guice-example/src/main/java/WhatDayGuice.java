import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

public class WhatDayGuice {
    public static void main(String[] args) {
        test();

        Injector injector = Guice.createInjector(
            new AbstractModule() {
                @Override
                public void configure() {
                    bind(Clock.class).toInstance(Clock.systemUTC());
                    bind(PrintStream.class).toInstance(System.out);
                }
            }
        );

        injector.getInstance(WhatDayGuice.class).printDay();
    }

    PrintStream printStream;
    Clock clock;

    @Inject
    WhatDayGuice(PrintStream printStream, Clock clock) {
        this.printStream = printStream;
        this.clock = clock;
    }

    void printDay() {

        DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("cccc")
            .withLocale(Locale.UK)
            .withZone(ZoneId.of("Europe/London"));

        printStream.println(formatter.format(Instant.now(clock)));
    }

    static void test() {
        // Given a Thursday in 2019
        Instant sept12 = DateTimeFormatter.ISO_INSTANT
            .parse("2019-09-12T16:04:00Z").query(Instant::from);
        ZoneId europe = ZoneId.of("Europe/London");
        Clock clock = Clock.fixed(sept12, europe);

        // And a PrintStream
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);

        // When we print the day
        WhatDayGuice wd = new WhatDayGuice(ps, clock);
        wd.printDay();

        // Then we printed Thursday!
        assert os.toString().equals("Thursday\n");
    }
}
