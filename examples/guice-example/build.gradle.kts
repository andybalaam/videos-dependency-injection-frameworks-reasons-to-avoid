import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    java
    id("com.github.johnrengelman.shadow") version "5.1.0"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.google.inject:guice:4.2.2")
}

tasks.withType<ShadowJar>() {
    manifest {
        attributes["Main-Class"] = "WhatDayGuiceNoConstr"
    }
}

