import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;

public class WhatDayTestable {
    public static void main(String[] args) {
        test();

        PrintStream ps = System.out;
        Clock clock = Clock.systemUTC();
        new WhatDayTestable(ps, clock).printDay();
    }

    PrintStream printStream;
    Clock clock;

    WhatDayTestable(PrintStream printStream, Clock clock) {
        this.printStream = printStream;
        this.clock = clock;
    }

    void printDay() {

        DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("cccc")
            .withLocale(Locale.UK)
            .withZone(ZoneId.of("Europe/London"));

        printStream.println(formatter.format(Instant.now(clock)));
    }

    static void test() {
        // Given a Thursday in 2019
        Clock clock = Clock.fixed(
            ISO_INSTANT.parse("2019-09-12T16:04:00Z").query(Instant::from),
            ZoneId.of("Europe/London")
        );

        // And a PrintStream
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);

        // When we print the day
        WhatDayTestable wd = new WhatDayTestable(ps, clock);
        wd.printDay();

        // Then we printed Thursday!
        assert os.toString().equals("Thursday\n");
    }
}
