import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class WhatDayIsIt {
    public static void main(String[] args) {
        new WhatDayIsIt().printDay();
    }

    void printDay() {

        DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("cccc")
            .withLocale(Locale.UK)
            .withZone(ZoneId.of("Europe/London"));

        System.out.println(formatter.format(Instant.now()));
    }
}
